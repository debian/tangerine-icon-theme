tangerine-icon-theme (0.26.debian-6) unstable; urgency=medium

  * QA upload.
  * debian/control.in: Dropped, we already no longer generate
    debian/control file from debian/control.in.
  * Apply "wrap-and-sort -abst".
  * debian/control:
    + Bump Standards-Version to 4.6.2.
    + Bump debhelper compat to v13.
    + Add new git packaging repo under Debian Salsa platform.
    + Do not list individual Build-Depends-Indep field to simplify
      packaging.
    + Drop explicit build-dependency on dpkg-dev, autoconf, automake,
      autotools-dev.
    + Drop alternative build-dependency on disappeared libmagick9-dev.

 -- Boyuan Yang <byang@debian.org>  Tue, 31 Jan 2023 16:36:30 -0500

tangerine-icon-theme (0.26.debian-5) unstable; urgency=medium

  * QA upload
  * Drop obsolete ubuntu-get-source rule (Closes: #917483)
  * Drop obsolete simple-patchsys rule
  * Switch to source format 3.0 (quilt)

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 28 Dec 2018 10:31:46 -0500

tangerine-icon-theme (0.26.debian-4) unstable; urgency=low

  * QA upload.

  [ Emilio Pozuelo Monfort ]
  * debian/control.in,
    debian/rules:
    + Use dpkg-vendor instead of lsb_release.

  [ Michael Biebl ]
  * Acknowledge NMU, thanks Andreas Beckmann.
  * Orphan package. Set the maintainer to Debian QA Group, see #824691.

 -- Michael Biebl <biebl@debian.org>  Wed, 18 May 2016 20:46:56 +0200

tangerine-icon-theme (0.26.debian-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/cleanup.sh: Do not run processes in background.  (Closes: #793161)

 -- Andreas Beckmann <anbe@debian.org>  Sat, 12 Sep 2015 18:26:08 +0200

tangerine-icon-theme (0.26.debian-3) unstable; urgency=high

  * Use "libmagickcore-dev | libmagick9-dev" as the imagemagick build-dep,
    i.e. unversionned as the experimental imagemagick provides libmagick9-dev
    and the versionned bdep would break there -- libmagickcore-dev is the new
    place where ImageMagick.pc is found, and this .pc file is only used by
    tangerine-icon-theme to check the version of imagemagick.

 -- Loic Minier <lool@dooz.org>  Wed, 03 Dec 2008 13:19:27 +0100

tangerine-icon-theme (0.26.debian-2) unstable; urgency=low

  * Fix some spacing.
  * Rename *.uuencode to *.uue.
  * Fix location of scalable start-here override for Debian.
  * Rewrite uue -> png conversion completely to not hardcode extension,
    location, directory names, to avoid a copy and to do the conversion in the
    post-install part.
  * Bump up Standards-Version to 3.8.0.
  * Drop changelog.ubuntu as the package was synced in Ubuntu.

 -- Loic Minier <lool@dooz.org>  Sun, 06 Jul 2008 19:27:19 +0200

tangerine-icon-theme (0.26.debian-1) unstable; urgency=low

  [ Alan Baghumian ]
  * New upstream release
  * Ported sng icon replacing method to uuencode/uudecode
    - Added uuencoded icons for Debian and Parsix
    - Added new 24x24 icons for Debian and Parsix
    - Removed sng icons
    - Dropped sng from build-deps
    - Updated debian/rules
  * Added a clean-up script to clean extra files before/after
    the build process
  * Added changelog.ubuntu
  * Added missing Debian 48x48 SVG icon
  * Updated copyright file to add debian tarball note

  [ Alan Baghumian ]
  * New upstream release
    - Does not depend on tango-icon-theme anymore
  * New upstream release:
    - dropped Tango icon theme inheritance.
    - new drive-* icons.
    - added recolored bit of g-i-t.
    - added a recolored tango icon theme audio-x-generic.
  * Added autoconf, automake, intltool to build-deps
  * Updated debian/rules to use the autogen.sh script
  * Updated watch file to seek *.tar.gz packages

  [ Loic Minier ]
  * Merge not uploaded changelog entries.
  * Cleanup whitespace in copyright.
  * Call $(SHELL) on clean-up.sh explicitely as the diff can't preserve
    executable permissions.
  * Build-depend on sharutils for uudecode.

 -- Alan Baghumian <alan@technotux.org>  Tue, 15 Jan 2008 11:56:20 +0330

tangerine-icon-theme (0.21-1) unstable; urgency=low

  [ Alan Baghumian ]
  * New upstream release
    - Removing GTK Tango icons from Tangerine as they are now
      in GNOME.
  * Merged with previous changelog entry
  * Updated Parsix logos

  [ Kilian Krause ]
  * Add get-orig-source target.

  [ Loic Minier ]
  * Use ubuntu-get-source.mk instead of gnome-get-source.mk; bump up
    gnome-pkg-tools build-dep to >= 0.12.2.

 -- Loic Minier <lool@dooz.org>  Tue, 31 Jul 2007 16:12:38 +0200

tangerine-icon-theme (0.20-1) unstable; urgency=low

  [ Alan Baghumian ]
  * Initial release, forked from Ubuntu (Closes: #420319).
  * Updated debian/control
     - Removed Ubuntu specific lines
     - GNOME team uploads
     - Changed maintainer
     - Added ${misc:Depends}, gnome-icon-theme and
       tango-icon-theme to deps
     - Added gnome-pkg-tools to build-deps
     - Removed hicolor-icon-theme from build-deps
     - Removed libgtk2.0-bin from deps, not needed
     - Bump debhelper dep to 5.0.0
     - Added Debian and Parsix specific svg and sng logos
     - Added libmagick9-dev and imagemagick build-deps
       required versions
     - Added librsvg2-dev, sng and lsb-release to
       build-deps
  * Added debian/watch and debian/control.in files
  * Updated debian/rules
     - Added simple-patchsys.mk and uploaders.mk
     - Added lsb_release distro detection method
     - Replaces Ubuntu logo with Debian or Parsix

  [ Loic Minier ]
  * Drop libgtk2.0-bin build-dep.

 -- Loic Minier <lool@dooz.org>  Mon, 07 May 2007 09:35:07 +0200

