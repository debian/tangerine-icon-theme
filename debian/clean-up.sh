#!/bin/sh

# Manually clean-up 
find ./ -name Makefile.in* | xargs rm -f >/dev/null
rm -f ./po/Makefile.in.in >/dev/null
rm -f ./configure >/dev/null
rm -f ./intltool-update.in >/dev/null
rm -f ./intltool-merge.in >/dev/null
rm -r ./intltool-extract.in >/dev/null
rm -f ./config.guess >/dev/null
rm -f ./config.sub >/dev/null
rm -f ./install-sh >/dev/null
rm -f ./missing >/dev/null
rm -f ./aclocal.m4 >/dev/null

